# JomoUniapp
快速开发JomoUniapp项目
> 快速开发uniapp项目，引入ColorUI,uView2.0,axios网络请求,全局路由uni-simple-router,Vuex,i18n中英文切换使用等，JomoDemo便于快速开发;

### 截图示例

![5](https://tva3.sinaimg.cn/mw690/006DFWgBgy1h0azlyzcosj31yu15vb29.jpg)

![6](https://tva3.sinaimg.cn/mw690/006DFWgBgy1h0azmx6o70j31zm167qp1.jpg)

### 项目基础搭建

+ [axios请求](http://www.axios-js.com/)
+ [uni-simple-router](https://github.com/SilurianYang/uni-simple-router)
+ [Vuex](https://vuex.vuejs.org/zh/guide/)


### 使用UI组件
+ [uniapp UI](https://github.com/dcloudio/uni-app)
+ [ColorUI](https://github.com/weilanwl/ColorUI)
+ [uView UI](https://github.com/YanxinNet/uView)



---
有需要可加wx我：ImGeomPa或进qq群[857064044](https://jq.qq.com/?_wv=1027&k=0C7oeCWW)交流